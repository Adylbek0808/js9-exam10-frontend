import { Container, CssBaseline } from '@material-ui/core';
import React from 'react';
import { Route, Switch } from 'react-router';
import Appbar from './components/UI/Appbar/Appbar';
import CreateNewsBlock from './containers/CreateNewsBlock/CreateNewsBlock';
import NewsBlock from './containers/NewsBlock/NewsBlock';

const App = () => (
  <>
    <CssBaseline />
    <header>
      <Appbar />
    </header>
    <main>
      <Container>
        <Switch>
          <Route path="/" exact component={NewsBlock} />
          <Route path="/createNews" exact component={CreateNewsBlock} />
          {/* <Route path={props.match.path} exact component={NewsFull}/> */}
        </Switch>
      </Container>

    </main>
  </>
)

export default App;

import { Grid, Paper, Typography } from '@material-ui/core';
import React from 'react';

const NewsFull = ({title, text, date}) => {
    return (
        <Grid item container>
            <Typography variant="h4">{title}</Typography>
            <Typography variant="subtitle1">{date}</Typography>
            <Grid item xs>
                {text}
            </Grid>
            <Grid item xs>
                <Paper>
                    Comments section
                </Paper>
            </Grid>
        </Grid>
    );
};

export default NewsFull;
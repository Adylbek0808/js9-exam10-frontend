import { Button, Grid, TextField } from '@material-ui/core';
import React, { useState } from 'react';

const CreateNewsForm = ({onSubmit}) => {
    const [state, setState] = useState({
        title: '',
        newsText: ''
    });

    const submitFormHandler = e => {
        e.preventDefault();
        
        onSubmit(state);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    return (
        <form onSubmit={submitFormHandler}>
            <Grid container direction="column" spacing={2}>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Title"
                        name="title"
                        value={state.title}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        fullWidth
                        multiline
                        rows={3}
                        variant="outlined"
                        label="News Text"
                        name="newsText"
                        value={state.newsText}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">
                        Create
                    </Button>
                </Grid>
            </Grid>

        </form>
    );
};

export default CreateNewsForm;
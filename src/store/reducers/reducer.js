import { FETCH_NEWS_FAILURE, FETCH_NEWS_REQUEST, FETCH_NEWS_SUCCESS } from '../actions/actions';

const initialState = {
    news:[],
    newsLoading: false
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return { ...state, newsLoading: true }
        case FETCH_NEWS_SUCCESS:
            return { ...state, newsLoading: false, news: action.posts };
        case FETCH_NEWS_FAILURE:
            return { ...state, newsLoading: false }
        default:
            return state
    }
}

export default reducer;
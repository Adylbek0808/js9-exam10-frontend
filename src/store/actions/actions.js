import axiosApi from "../../axiosApi";

export const FETCH_NEWS_REQUEST = "FETCH_NEWS_REQUEST";
export const FETCH_NEWS_SUCCESS = "FETCH_NEWS_SUCCESS";
export const FETCH_NEWS_FAILURE = "FETCH_NEWS_FAILURE";


export const CREATE_NEWS_SUCCESS = "CREATE_NEWS_SUCCESS";


export const fetchNewsRequest = () => ({ type: FETCH_NEWS_REQUEST });
export const fetchNewsSuccess = posts => ({ type: FETCH_NEWS_SUCCESS, posts });
export const fetchNewsFailure = () => ({ type: FETCH_NEWS_FAILURE });

export const createNewsSuccess = () => ({ type: CREATE_NEWS_SUCCESS });


export const fetchNews = () => {
    return async dispatch => {
        try {
            dispatch(fetchNewsRequest());
            const response = await axiosApi.get('/news');
            console.log(response.data)
            dispatch(fetchNewsSuccess(response.data))
        } catch (e) {
            dispatch(fetchNewsFailure());
            console.log('Could not fetch NEWS')
        }
    }
}

export const createNews = newsData => {
    return async dispatch => {
        await axiosApi.post('/news', newsData);
        dispatch(createNewsSuccess());
    }
}

export const deleteNews = id => {
    return async () => {
        await axiosApi.delete('/news/' + id);

    }
}
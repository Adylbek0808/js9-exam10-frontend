import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import CreateNewsForm from '../../components/CreateNewsForm/CreateNewsForm';
import { createNews } from '../../store/actions/actions';

const CreateNewsBlock = () => {
    const dispatch = useDispatch();

    const onNewsFormSubmit = async newsData => {
       await dispatch(createNews(newsData));
    
    }

    return (
        <Grid container direction="column">
            <Grid item xs>
                <Typography variant="h4">Enter News</Typography>
            </Grid>
            <Grid item xs>
                <CreateNewsForm onSubmit={onNewsFormSubmit} />
            </Grid>
        </Grid>
    );
};

export default CreateNewsBlock;
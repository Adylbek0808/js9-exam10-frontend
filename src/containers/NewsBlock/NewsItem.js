import { Button, Grid, makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteNews } from '../../store/actions/actions';

const useStyles = makeStyles(theme => ({

    newsItem: {
        margin: theme.spacing(2),
        border: "1px solid blue",
        borderRadius: "3px"
    }

}))



const NewsItem = ({ text, time, id, history }) => {
    const dispatch = useDispatch();

    const classes = useStyles()

    const deleteThisNews = async (id) => {
        console.log(id);
        await dispatch(deleteNews(id))
    }

    return (
        <Grid item container direction="column" spacing={3} className={classes.newsItem}>
            <Grid item xs >
                <Typography variant='h6'> {text}</Typography>
            </Grid>
            <Grid item >
                <Typography display="inline">{time}</Typography>
                <Link to={'/news/' + id}>See full news</Link>
                <Button variant="contained" onClick={() => deleteThisNews(id)}>Delete</Button>
            </Grid>
        </Grid>
    );
};

export default NewsItem;
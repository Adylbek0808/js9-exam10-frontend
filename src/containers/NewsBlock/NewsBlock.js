
import { Button, Grid, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchNews } from '../../store/actions/actions';
import NewsItem from './NewsItem';

const useStyles = makeStyles(theme => ({
    baseGrid: {
        margin: theme.spacing(0, 'auto'),
        width: '80%'
    }
}))

const NewsBlock = () => {

    const dispatch = useDispatch()
    const posts = useSelector(state => state.news)
    const classes = useStyles();

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch]);


    return (
        <Grid container direction="column" className={classes.baseGrid}>
            <Grid item xs>
                <Typography variant="h6">News</Typography>
                <Button variant="outlined" component={Link} to="/createNews"> Create News</Button>
            </Grid>

            {posts.map(item => (
                <NewsItem
                    text={item.title}
                    time={item.datetime}
                    key={item.id}
                    id = {item.id}


                />
            ))}



        </Grid>
    );
};

export default NewsBlock;